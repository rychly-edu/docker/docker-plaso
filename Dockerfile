# Based on https://github.com/log2timeline/plaso/blob/master/config/docker/Dockerfile
# See also https://plaso.readthedocs.io/en/latest/sources/developer/Dependencies.html#ubuntu

ARG UBUNTU_VERSION
FROM ubuntu:${UBUNTU_VERSION:-focal}

MAINTAINER Marek Rychly <marek.rychly@gmail.com>

ARG GIT_TAG="20210412"
ARG GIFT_PPA_TRACK="stable"
ARG PYTHON="python3"
ARG DEP_SCRIPT="ubuntu_install_plaso"

RUN true \
&& export DEBIAN_FRONTEND=noninteractive \
&& apt-get -y update \
&& apt-get -y install software-properties-common apt-transport-https locales wget \
# Set terminal to UTF-8 by default
&& locale-gen en_US.UTF-8 \
&& update-locale LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 \
#
# GIT release
#
&& wget -O - "https://github.com/log2timeline/plaso/archive/refs/tags/${GIT_TAG}.tar.gz" | tar xzC /opt \
&& ln -vs /opt/plaso-* /opt/plaso \
&& sed -i -e "s|ppa:gift/dev|ppa:gift/${GIFT_PPA_TRACK}|g" -e 's|sudo ||g' -e 's|python-sphinx|python3-sphinx|g' "/opt/plaso/config/linux/${DEP_SCRIPT}.sh" \
&& if [ "${PYTHON}" = "python3" ]; then sed -i -e 's|python-sphinx|python3-sphinx|g' "/opt/plaso/config/linux/${DEP_SCRIPT}.sh"; fi \
&& cd /opt/plaso && "./config/linux/${DEP_SCRIPT}.sh" include-development include-test && cd - \
#
# ENABLED: install from the cloned repository
&& cd /opt/plaso && "${PYTHON}" ./setup.py install && cd - \
#
# DISABLED: install from the PPA instead of the cloned repository
#&& apt-get -y install python-plaso plaso-tools locales \
#&& wget -O /usr/local/bin/plaso-switch.sh "https://github.com/log2timeline/plaso/raw/${GIT_TAG}/config/docker/plaso-switch.sh" \
#&& chmod -v a+x /usr/local/bin/plaso-switch.sh \
#
# STABLE release from packages
# Run log2timeline on artifacts stored in /data/artifacts with:
# docker run -ti -v /data/:/data/ <container_id> --entrypoint /usr/local/bin/plaso-switch.sh log2timeline /data/results/result.plaso /data/artifacts
#
# keep wget as we will use it later in production to download data for analysis
#&& apt-get -y autoremove --purge wget \
&& apt-get clean \
&& rm -rf /var/cache/apt/* /var/lib/apt/lists/*

ENV \
LANG="en_US.UTF-8" \
LC_ALL="en_US.UTF-8"

VOLUME ["/data"]
